<?php
/*
Template Name: Location
*/
?>
<?php include "includes/top.php";?>
  <body id="contact">
  
    <?php include "includes/header.php";?>

<div id="content" class="location">

  <div id="gridpagetop">
          
          <?php the_post_thumbnail('post-thumbnail'); ?>
       
          <div id="pagetext">
          
          <h2><?php the_title(); ?></h2>
              
                      <?php if (have_posts()) : ?>
                      <?php while (have_posts()) : the_post(); ?>
                      <?php the_content(); ?>
                      <?php endwhile; ?>
                      <?php else : ?>
							       <h2 class="center">Not Found</h2>
							       <p class="center">Sorry, but you are looking for something that isn't here.</p>
							       <?php endif; ?>
		       
          </div>
          
          <div class="clear"></div>

  </div>
       
        <div class="clear"></div>
        
        <div id="footer">
            
            <?php include "includes/newspanel.php";?>
            
            <?php include "includes/footer.php";?>