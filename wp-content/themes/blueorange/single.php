<?php include "includes/top.php";?>
  <body>
  
    <?php include "includes/header.php";?>

<div id="content" class="newspage">

    <div id="panelbody">
          
                      <?php if (have_posts()) : ?>
                      <?php while (have_posts()) : the_post(); ?>
                      
                      <div class="postthumbnail">
                        <?php the_post_thumbnail('post-thumbnail'); ?>
                      </div>
                      <span class="date"><?php the_time('F jS, Y') ?></span>
                      <h2><?php the_title(); ?></h2>
                      <?php the_content(); ?>
                      <div class="clear"></div>
                      <?php endwhile; ?>
                      <?php else : ?>
							       <h2 class="center">Not Found</h2>
							       <p class="center">Sorry, but you are looking for something that isn't here.</p>
							       <?php endif; ?>
							       
							       <div class="pagelinks">
							         <div class="nav-previous"><?php previous_post_link( '%link', __( '<span class="meta-nav">&larr;</span> Previous item', 'blueorange' ) ); ?></div>
							         <div class="nav-next"><?php next_post_link( '%link', __( 'Next item <span class="meta-nav">&rarr;</span>', 'Next item' ) ); ?></div>
							         <div class="clear"></div>
							       </div>
      
    </div>

        <div class="clear"></div>
        
        <div id="footer">
            
            <?php include "includes/newspanel.php";?>
            
            <?php include "includes/footer.php";?>