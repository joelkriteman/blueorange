<?php
/*
Template Name: Sample Grid
*/
?>
<?php include "includes/top.php";?>
  <body id="sample">
  
    <?php include "includes/header.php";?>

<div id="content" class="threecolumn">
      
      <div id="gridpagetop">
          
          <?php the_post_thumbnail('post-thumbnail'); ?>
       
          <div id="pagetext">
          
              <h2><?php the_title(); ?></h2>
              
                      <?php if (have_posts()) : ?>
                      <?php while (have_posts()) : the_post(); ?>
                      <?php the_content(); ?>
                      <?php endwhile; ?>
                      <?php else : ?>
							       <h2 class="center">Not Found</h2>
							       <p class="center">Sorry, but you are looking for something that isn't here.</p>
							       <?php endif; ?>
							       
						
							       
          </div>
          
        <div class="clear"></div>
          
      </div>
          
          <div class="clear"></div>
      
          <div id="homethumbs" class="grid">
          <ul>
            <li class="left">
              <div>
                <div id="cimy_div_id_1">Loading images...</div>
<noscript>
	<div id="cimy_div_id_1_nojs">
		<img id="cimy_img_id" src="http://localhost/blueorange/wp-content/Cimy_Header_Images/1/newspaper-stack-thumb5123882.jpg" alt="" />
	</div>
</noscript>
                <span class="homethumbtext"><?php the_field('column_1_title'); ?></span>
                
                <?php the_field('column_1_text'); ?>

              </div>
            </li>
             <li class="threecentre">
              <div>
                <img src="<?php the_field('column_2_image'); ?>" alt="" /><br />
                <span class="homethumbtext"><?php the_field('column_2_title'); ?></span>
                
                <?php the_field('column_2_text'); ?>

              </div>
            </li>
             <li>
              <div>
                <img src="<?php the_field('column_3_image'); ?>" alt="" /><br />
                <span class="homethumbtext"><?php the_field('column_3_title'); ?></span>
                
                <?php the_field('column_3_text'); ?>

              </div>
            </li>
          </ul>
          
          <div class="clear"></div>
          
        </div>

        
       
        <div class="clear"></div>
        
        <div id="footer">
            
            <?php include "includes/newspanel.php";?>
            
            <?php include "includes/footer.php";?>