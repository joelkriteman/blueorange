<div id="wrapper">
      <div id="header">
          <img id="logo" src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="logo" />
          <div id="officesolutions">
            office solutions ltd
          </div>
          
          <div id="headerdetails">
            01883 333 546<br />
            <a href="mailto:enquiries@blueorangeosl.co.uk">enquiries@blueorangeosl.co.uk</a>
          </div>
          
          <div id="strapline">
            A refreshing change for your office
          </div>
          
          <div id="navigation">
									
								<ul id="nav">
											<li id="nav-home">
												<a href="<?php bloginfo('home'); ?>" class="nav-home">Home</a>
												  <ul>
													 <li class="nav-home-sub"><a href="<?php bloginfo('home'); ?>">About Us</a></li>
													 <li class="nav-home-sub"><a href="<?php bloginfo('home'); ?>/News">News</a></li>
												  </ul>
											</li>
											<li id="nav-furniture">
												<a href="<?php bloginfo('home'); ?>/furniture">Furniture</a></li>
											<li id="nav-print"><a href="<?php bloginfo('home'); ?>/print">Print</a></li>
											<li id="nav-signage">
												<a href="<?php bloginfo('home'); ?>/signage">Signage</a></li>
											<li id="nav-fitout">
												<a href="<?php bloginfo('home'); ?>/fit-out">Fit out</a></li>
											<li id="nav-stationery">
												<a href="<?php bloginfo('home'); ?>/stationery">Stationery</a></li>
											<li id="nav-sample">
										   <a href="<?php bloginfo('home'); ?>/samples">Samples</a></li>
										  <li id="nav-projects">
										    <a href="<?php bloginfo('home'); ?>/projects">Projects</a></li>
											<li id="nav-contact">
												<a href="#">Contact Us</a>
												  <ul>
													 <li class="nav-contact-sub"><a href="<?php bloginfo('home'); ?>/location">Location</a></li>
													 <li class="nav-contact-sub"><a href="<?php bloginfo('home'); ?>/enquiries">Enquiries</a></li>
													 <li class="nav-contact-sub"><a href="<?php bloginfo('home'); ?>/account-application">Account Form</a></li>
												  </ul>
											</li>
								</ul>
											
											
						</div>
						
      </div>