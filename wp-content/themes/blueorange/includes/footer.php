            <div id="footercontact">
              <div id="footeraddress">
                BlueOrange OSL&nbsp;&nbsp;|&nbsp;&nbsp;UKGlobal, 3rd Floor, Quadrant House, Caterham, Surrey, CR3 6TR&nbsp;&nbsp;|&nbsp;&nbsp;<a href="mailto:enquiries@blueorangeosl.co.uk">enquiries@blueorangeosl.co.uk</a>
              </div>
              <div id="footercarciofino">
                website by <a href="http://www.carciofino.com" target="_blank">carciofino</a>
              </div>

            </div>
        </div>
        
      </div>
      <div class="clear"></div>
    </div>
  <?php wp_footer(); ?>
  </body>
</html>