            <div id="newspanel">
                
										<ul>
											<li class="left">
											 <h4><a href="<?php bloginfo('home'); ?>/category/newsflash">Newsflash</a></h4>
											  <?php
									 global $post;
									 $myposts = get_posts('numberposts=1&cat=8');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>
											 <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
											   <div class="blurb"><?php the_field('post_summary'); ?></div>
										 <?php endforeach; ?>
										  </li>
										  
										  <li>
										    <h4><a href="<?php bloginfo('home'); ?>/category/special-offers">Special Offers</a></h4>
											  <?php
									 global $post;
									 $myposts = get_posts('numberposts=1&cat=9');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>						 
											 <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
											   <div class="blurb"><?php the_field('post_summary'); ?></div>
										 <?php endforeach; ?>
										  </li>
										  
										  <li>
										    <h4><a href="<?php bloginfo('home'); ?>/category/product-news">Product News</a></h4>
											  <?php
									 global $post;
									 $myposts = get_posts('numberposts=1&cat=5');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>
											 <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
											   <div class="blurb"><?php the_field('post_summary'); ?></div>
										 <?php endforeach; ?>
										  </li>
										  
										  <li>
										  <h4><a href="<?php bloginfo('home'); ?>/category/company-news">Company News</a></h4>
											  <?php
									 global $post;
									 $myposts = get_posts('numberposts=1&cat=6');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>
											 <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
											   <div class="blurb"><?php the_field('post_summary'); ?></div>
										 <?php endforeach; ?>
										  </li>
										  
										  <li>
										  <h4><a href="<?php bloginfo('home'); ?>/category/industry-news">Industry News</a></h4>
											  <?php
									 global $post;
									 $myposts = get_posts('numberposts=1&cat=7');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>
											 <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
											   <div class="blurb"><?php the_field('post_summary'); ?></div>
										 <?php endforeach; ?>
										  </li>
										 
										</ul>
								
									

                
                <div class="clear"></div>
                
                									 
            </div>