<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'
      'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
  <head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
      <meta name="Description" content="" />
	   <meta name="keywords" content="" />
	   <meta name="url" content="http://www.blueorange.co.uk" />
	   <meta name="rating" content="Safe For Kids" />
	   <meta name="revisit-after" content="30 days" />
	   <meta name="robots" content="all" />
	   <meta name="copyright" content="Copyright &copy;BlueOrange" />
	   <title>Blue Orange</title>
	   <link href="<?php bloginfo( 'stylesheet_url' ); ?>" rel="stylesheet" type="text/css"  media="screen" />
  <?php wp_head(); ?>
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/navigation.css" />
  </head>