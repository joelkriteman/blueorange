<?php
/*
Template Name: News Grid
*/
?>
<?php include "includes/top.php";?>
  <body>
  
    <?php include "includes/header.php";?>

<div id="content">
     
     <div id="gridpagetop">
          
          <?php the_post_thumbnail('post-thumbnail'); ?>
       
          <div id="pagetext">
          
              <h2><?php the_title(); ?></h2>
              
                      <?php if (have_posts()) : ?>
                      <?php while (have_posts()) : the_post(); ?>
                      <?php the_content(); ?>
                      <?php endwhile; ?>
                      <?php else : ?>
							       <h2 class="center">Not Found</h2>
							       <p class="center">Sorry, but you are looking for something that isn't here.</p>
							       <?php endif; ?>
							       
						
							       
          </div>
          
        <div class="clear"></div>
          
      </div>
          
          <div class="clear"></div> 
      
          
         
         <div id="newspanel">
                
										<ul>
											
											<li class="left">
										    <h4>Newsflash</h4>
											  <?php
									 global $post;
									 $myposts = get_posts('numberposts=3&cat=8');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>
											 <p class="articletitle"><?php the_title(); ?></p>
											 <div class="clear"></div>
											 <?php the_post_thumbnail('post-thumbnail'); ?>
											 <div class="clear"></div>
											 <?php the_excerpt(); ?>
											   <div class="readlinks">
							             <span class="nav-next"><a href="<?php the_permalink(); ?>">Read More >></a></span>
							             <div class="clear"></div>
							           </div>
  										 <?php endforeach; ?>
  										 <a class="allnews" href="<?php bloginfo('home'); ?>/category/newsflash">All Newsflashes >></a>
  										 <div class="clear"></div>
										  </li>
										  
										  
										  <li>
										    <h4>Special Offers</h4>
											  <?php
									 global $post;
									 $myposts = get_posts('numberposts=3&cat=9');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>
											 <p class="articletitle"><?php the_title(); ?></p>
											 <div class="clear"></div>
											 <?php the_post_thumbnail('post-thumbnail'); ?>
											 <div class="clear"></div>
											 <?php the_excerpt(); ?>
											   <div class="readlinks">
							             <span class="nav-next"><a href="<?php the_permalink(); ?>">Read More >></a></span>
							             <div class="clear"></div>
							           </div>
  										 <?php endforeach; ?>
  										 <a class="allnews" href="<?php bloginfo('home'); ?>/category/special-offers">All Special Offers >></a>
  										 <div class="clear"></div>
										  </li>
										  
										  
										  <li>
										    <h4>Product News</h4>
											  <?php
									 global $post;
									 $myposts = get_posts('numberposts=3&cat=5');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>
											<p class="articletitle"><?php the_title(); ?></p>
											 <div class="clear"></div>
											 <?php the_post_thumbnail('post-thumbnail'); ?>
											 <div class="clear"></div>
											 <?php the_excerpt(); ?>
											   <div class="readlinks">
							             <span class="nav-next"><a href="<?php the_permalink(); ?>">Read More >></a></span>
							             <div class="clear"></div>
							           </div>
  										 <?php endforeach; ?>
  										 <a class="allnews" href="<?php bloginfo('home'); ?>/category/product-news">All Product News >></a>
  										 <div class="clear"></div>
										  </li>
										  
										  
										  <li>
											 <h4>Company News</h4>
											  <?php
									 global $post;
									 $myposts = get_posts('numberposts=3&cat=6');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>
											 <p class="articletitle"><?php the_title(); ?></p>
											 <div class="clear"></div>
											 <?php the_post_thumbnail('post-thumbnail'); ?>
											 <div class="clear"></div>
											 <?php the_excerpt(); ?>
											   <div class="readlinks">
							             <span class="nav-next"><a href="<?php the_permalink(); ?>">Read More >></a></span>
							             <div class="clear"></div>
							           </div>
  										 <?php endforeach; ?>
  										 <a class="allnews" href="<?php bloginfo('home'); ?>/category/company-news">All Company News >></a>
  										 <div class="clear"></div>
										  </li>
										  
										  
										  <li>
										    <h4>Industry News</h4>
											  <?php
									 global $post;
									 $myposts = get_posts('numberposts=3&cat=7');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>						 
											 <p class="articletitle"><?php the_title(); ?></p>
											 <div class="clear"></div>
											 <?php the_post_thumbnail('post-thumbnail'); ?>
											 <div class="clear"></div>
											 <?php the_excerpt(); ?>
											   <div class="readlinks">
							             <span class="nav-next"><a href="<?php the_permalink(); ?>">Read More >></a></span>
							             <div class="clear"></div>
							           </div>
  										 <?php endforeach; ?>
  										 <a class="allnews" href="<?php bloginfo('home'); ?>/category/industry-news">All Industry News >></a>
  										 <div class="clear"></div>
										  </li>

										</ul>
								                
                <div class="clear"></div>
                
                									 
            </div>

        
       
        <div class="clear"></div>
        
        <div id="footer">
            
            <?php include "includes/footer.php";?>