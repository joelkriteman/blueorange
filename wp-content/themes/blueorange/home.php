<?php
/*
Template Name: Home
*/
?>
<?php include "includes/top.php";?>
  <body id="home">
  
    <?php include "includes/header.php";?>

<div id="content">
          <div id="cimy_div_id_0">Loading images...</div>

<noscript>
	<div id="cimy_div_id_0_nojs">
		<img id="cimy_img_id" src="<?php bloginfo('home'); ?>/wp-content/Cimy_Header_Images/0/home1.jpg" alt="" />
	</div>
</noscript>

          
          <div id="hometext">
                      <?php if (have_posts()) : ?>
                      <?php while (have_posts()) : the_post(); ?>
                      <?php the_content(); ?>
                      <?php endwhile; ?>
                      <?php else : ?>
							       <h2 class="center">Not Found</h2>
							       <p class="center">Sorry, but you are looking for something that isn't here.</p>
							       <?php endif; ?>
          </div>

        
        <div id="homethumbs">
          <ul>
            <li class="left">
              <div>
                <a href="<?php bloginfo('home'); ?>/furniture"><img src="<?php bloginfo('template_directory'); ?>/images/furniture.jpg" alt="" /></a>
              </div>
            </li>
            <li>
              <div>
                <a href="<?php bloginfo('home'); ?>/print"><img src="<?php bloginfo('template_directory'); ?>/images/print.jpg" alt="" /></a>
              </div>
            </li>
            <li>
              <div>
                <a href="<?php bloginfo('home'); ?>/signage"><img src="<?php bloginfo('template_directory'); ?>/images/signage.jpg" alt="" /></a>
              </div>
            </li>
            <li>
              <div>
                <a href="<?php bloginfo('home'); ?>/fit-out"><img src="<?php bloginfo('template_directory'); ?>/images/fitout.jpg" alt="" /></a>
              </div>
            </li>
            <li>
              <div>
                <a href="<?php bloginfo('home'); ?>/stationery"><img src="<?php bloginfo('template_directory'); ?>/images/stationery.jpg" alt="" /></a>
              </div>
            </li>
          </ul>
          
          <div class="clear"></div>
          
        </div>
        <div class="clear"></div>
        
        <div id="footer">
            
            <?php include "includes/newspanel.php";?>
            
            <?php include "includes/footer.php";?>