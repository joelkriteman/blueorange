<?php include "includes/top.php";?>
  <body>
  
    <?php include "includes/header.php";?>

<div id="content" class="newspage">

  <div id="panelbody">
                      <?php if (have_posts()) : ?>
                      <?php while (have_posts()) : the_post(); ?>
                      
                      <div class="postthumbnail">
                        <?php the_post_thumbnail('post-thumbnail'); ?>
                      </div>
                      <span class="date"><?php the_time('F jS, Y') ?></span>
                      <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                      <?php the_excerpt(); ?>
                      <div class="pagelinks">
							         <span class="nav-next"><a href="<?php the_permalink(); ?>">Read More >></a></span>
							         <div class="clear"></div>
							       </div>
                      <div class="clear"></div>
                      <?php endwhile; ?>
                      <?php else : ?>
							       <h2 class="center">Not Found</h2>
							       <p class="center">Sorry, but you are looking for something that isn't here.</p>
							       <?php endif; ?>
							       
							       <div class="pagelinks">
							         <div class="nav-next"><?php previous_posts_link('Newer entries &rarr;') ?></div>
							         <div class="nav-previous"><?php next_posts_link('&larr; Older entries') ?></div>
							         <div class="clear"></div>
							       </div>
  </div>

        <div class="clear"></div>
        
        <div id="footer">
            
            <?php include "includes/newspanel.php";?>
            
            <?php include "includes/footer.php";?>