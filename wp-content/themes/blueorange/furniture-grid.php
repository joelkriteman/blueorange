<?php
/*
Template Name: Furniture Grid
*/
?>
<?php include "includes/top.php";?>
  <body id="furniture">
  
    <?php include "includes/header.php";?>

<div id="content">
      
      <div id="gridpagetop">
         
         <a href="http://online.fliphtml5.com/pwmns/ufuo/" target="_blank" > 
          <?php the_post_thumbnail('post-thumbnail'); ?>
        </a>
       
          <div id="pagetext">
          
              <h2><?php the_title(); ?></h2>
              
                      <?php if (have_posts()) : ?>
                      <?php while (have_posts()) : the_post(); ?>
                      <?php the_content(); ?>
                      <?php endwhile; ?>
                      <?php else : ?>
							       <h2 class="center">Not Found</h2>
							       <p class="center">Sorry, but you are looking for something that isn't here.</p>
							       <?php endif; ?>
							       
							       <p class="click">&larr;&nbsp;Click on catalogue image to open</p>
							  
							 <a class="furnituredownload" href="https://fliphtml5.com/download/download-pdf-file.php?str=x0DZh9GTud3bENXamgjM1ATNwITPkl0av9mY" target="_blank" >Download Brochure >></a>
							 
          </div>
          
        <div class="clear"></div>
          
      </div>
          
          <div class="clear"></div>
          
          <div id="homethumbs" class="grid">
          <ul>
            <li class="left">
              <div>
                <img src="<?php the_field('column_1_image'); ?>" alt="" /><br />
                <span class="homethumbtext"><?php the_field('column_1_title'); ?></span>
                
                <?php the_field('column_1_text'); ?>

              </div>
            </li>
             <li>
              <div>
                <img src="<?php the_field('column_2_image'); ?>" alt="" /><br />
                <span class="homethumbtext"><?php the_field('column_2_title'); ?></span>
                
                <?php the_field('column_2_text'); ?>

              </div>
            </li>
             <li>
              <div>
                <img src="<?php the_field('column_3_image'); ?>" alt="" /><br />
                <span class="homethumbtext"><?php the_field('column_3_title'); ?></span>
                
                <?php the_field('column_3_text'); ?>

              </div>
            </li>
             <li>
              <div>
                <img src="<?php the_field('column_4_image'); ?>" alt="" /><br />
                <span class="homethumbtext"><?php the_field('column_4_title'); ?></span>
                
                <?php the_field('column_4_text'); ?>

              </div>
            </li>
             <li>
              <div>
                <img src="<?php the_field('column_5_image'); ?>" alt="" /><br />
                <span class="homethumbtext"><?php the_field('column_5_title'); ?></span>
                
                <?php the_field('column_5_text'); ?>

              </div>
            </li>
            
          </ul>
          
          <div class="clear"></div>
          
        </div>

        
       
        <div class="clear"></div>
        
        <div id="footer">
            
            <?php include "includes/newspanel.php";?>
            
            <?php include "includes/footer.php";?>