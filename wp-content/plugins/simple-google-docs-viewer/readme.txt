=== Simple Google Docs Viewer ===
Contributors: maor, illuminea
Tags: google-docs, embed-pdf, documents
Requires at least: 3.0
Tested up to: 3.5
Stable tag: 1.0
License: GPLv2 or later

Enables you to easily embed documents with Google Docs Viewer - that are supported by Google Docs (PDF/DOC/DOCX/PPTX/etc).

== Description ==

Enables you to easily embed documents with Google Docs Viewer - that are supported by Google Docs (PDF/DOC/DOCX/PPTX/etc) with a simple shortcode. `[gviewer]`

== Installation ==

Add via the plugin installer, or download the ZIP file and upload from the "Upload" tab.


== Changelog ==

= 1.0 =

* Initial release