=== Plugin Name ===
Contributors: 1000grad
Donate link: 
Tags: epaper pdf newspaper ebook catalogue pageflip blätterkatalog flashflip emagazine
Requires at least: 3.4
Tested up to: 3.5.1
Stable tag: 1.3.4
License: GPLv2 or later

Easily create browsable ePapers within Wordpress.

Konvertieren Sie Ihre PDF in ein blätterbares Web-Dokument und binden Sie es mit einem Widget ein. 

== Description ==

In only a few simple steps you can create <b>a browsable ePaper</b> from scratch, simply by providing a PDF document. Thanks to our up to date <b>HTML5 viewer</b> your ePaper will look brilliant and sharp, even on Android, iPad and other devices.

In nur wenigen Schritten erstellen Sie sich aus Ihrer PDF-Datei <b>ein blätterbares Dokument für Ihre Webseite</b>.  Auch auf Android, iPad & Co. macht Ihr ePaper in der automatischen HTML5-Darstellung einen sehr guten Eindruck.

= Information =

Convert your PDF document to an online document, by using our 1000° web service and embed it via wordpress widget or shortcode in your page.
Regular web browsers make use of our enhanced flash viewer, whereas mobile devices running on Android and iOS will use out up-to-date HTML5 viewer.
Your converted PDF document can be updated, or replaced anytime. In order to do that, simply upload the new PDF document to your existing ePaper channel.
Take our a closer look at our free beta-service and take your ePaper channel for a test-drive.
Additional publishing channels, for instance for more widgets can be added on demand.

= Beschreibung =

Konvertieren Sie Ihre PDF mit dem Webservice von 1000° in ein blätterbares Web-Dokument. Binden Sie es einfach per Widget oder Shortcode ein. Desktop-Browser stellen das ePaper mittels Flash dar. Auf mobilen Endgeräten (iOS,Android) macht Ihr ePaper in der automatischen HTML5-Darstellung einen sehr guten Eindruck.
Das Dokument in Ihrem Widget können Sie beliebig oft aktualisieren oder austauschen. Dafür laden Sie Ihre neue PDF einfach in den bestehenden ePaper-Kanal. 
Testen Sie diese kostenlose Beta-Version und nutzen Sie unseren Service mit einen kostenlosen ePaper Kanal.
Weitere Kanäle z.B. für zusätzliche Widget-Plätze können bei Bedarf hinzugefügt werden.



== Installation ==

= Installation Instructions =

* Install and activate this plugin through the 'Plugins' menu in WordPress
* Confirm email registration after activation in the settings, you will get one channel for free for the next 12 months.
* Upload PDF at the 1000°grad plugin page.
* Set Title and language during upload progress.
* add post or page with that ePaper Channel or use shortcode in existing posts or pages.
* explore the posibility to use the ePaper widgets.
* Enjoy!

= Instruktionen =

*  Laden Sie das Wordpress Plugin aus dem Wordpress Marketplace herunter und aktivieren es.
*  Registrieren Sie sich für die Nutzung des Dienstes. Als Dankeschön erhalten Sie einen Kanal kostenlos für die Dauer von 12 Monaten.
*  Laden Sie Ihre PDF-Datei hoch. Ihre ePaper-Ausgabe für Web und Mobile wird direkt erstellt.
*  Verlinken Sie Ihre ePaper-Ausgabe in Ihrem Blog. Dazu haben Sie folgende Möglichkeiten:
    * Verlinkung via Widget
    * Verlinkung als Wordpress Kürzel (Shortcode)
    * Verlinkung per Textlink
    * Verlinkung per HTML-Code
*    Aktualisieren Sie Ihren ePaper-Kanal oder buchen Sie neue Kanäle hinzu wenn Sie mehrere ePaper-Ausgaben gleichzeitig darstellen möchten.


== Frequently Asked Questions ==

= How to get started? =
 
Download the 1000° ePaper plugin from wordpress marketplace to your wordpress installation. The installation process is fully automated. The first time you access the newly created 1000° ePaper page in your control panel, you will be asked for registration. Registering with our service is free of charge and without obligation, but yet necessary to provide every user with their respective ePaper-publication. After successful registration you can immediately start creating your ePaper-publications and embedding them into your blog.
 
= Are there any costs for the use of this plugin? =
 
Using this plugin is free of charge and without obligation. For the duration of the beta phase, every user is provided with a free ePaper channel to publish documents and evaluate our service. As a thank-you gift for our dedicated beta testers, they receive a free channel for 12 months even after the completion of the beta. Further channel may be purchased in the final release by clicking the upgrade button.
 
= How long is the beta phase intended to last? =
  
The beta phase will run for approximately 3 months, followed by the 1.0 release version. For the duration of the beta phase every registered (=beta tester) user will be provided with a free channel to evaluate our service. The free channel may be used for a period of 12 months after the completion of the beta phase. Afterwards the channel will either be deactivated, or remain active if you wish continue. The beta trial DOES NOT result in an automatic service charge or any other obligation. Please rest assured, you don’t have to pay anything unless explicitly want to.

= Can I upload bigger sized PDFs too? =

The upload size is mostly limited by your wordpress installation or installed PHP. Your can increase that value by editing wp-admin/.htaccess or php.ini with these values:
* php_value upload_max_filesize 50M
* php_value post_max_size 50M
* php_value max_execution_time 200
* php_value max_input_time 200

or have a look at http://php.net/manual/en/features.file-upload.php

= Can I integrate other 1000° products, i.e. purchased ePapers? =

When using shortcodes you can also link to existing ePapers. Use [ePaper url=http://....] with the URL to your ePaper on your own server.

= Wie kann ich starten? =

Laden Sie sich das Plugin direkt aus dem Wordpress Marketplace herunter. Die Installation des Plugins erfolgt dabei automatisch. Wenn Sie das Plugin über den Menüpunkt 1000°ePaper aufrufen werden Sie erstmalig zur Registrierung aufgefordert. Die Registrierung ist kostenlos und unverbindlich, aber notwendig, damit wir den Dienst zur Verfügung stellen können und jedem Nutzer die richtigen ePaper-Ausgaben ausliefern können. Nach erfolgter Registrierung können Sie direkt ePaper-Ausgaben erstellen und mit Ihrem Blog verknüpfen.

= Ist die Nutzung des Plugins kostenpflichtig? =

Die Nutzung des Plugins ist kostenlos und unverbindlich. Während der Dauer der Beta-Phase erhält jeder Nutzer einen Kanal kostenlos zum Ausprobieren. Als Dankeschön für die Teilnahme erhält jeder Beta-Tester darüber hinaus ein Jahr lang einen Kanal kostenlos zur Verfügung. Weitere Kanäle können später (im finalen Release) kostenpflichtig hizugebucht werden. Über einen Kaufen-Button können dann weitere Kanäle gebucht werden.

= Wie läuft die Beta-Phase ab? =

Die Beta-Phase läuft 3 Monate und geht dann in die Version 1.0 über. Für die Dauer der Beta-Phase erhält jeder registrierte Nutzer (=Beta-Tester) einen Kanal zum Ausprobieren. Außerdem kann jeder Beta-Tester den Kanal weiterhin kostenlos für 12 Monate nutzen. Danach wird der Kanal abgeschaltet oder kann auf Wunsch weiter betrieben werden. Der Beta-Test geht NICHT automatisch in einen kostenpflichtigen Dienst über. Also keine Angst - Sie zahlen nichts, solange Sie den Kaufbutton geklickt haben.

= Kann man auch größere PDF Dateien hochladen =

Die Upload Begrenzung erfolgt durch die Voreinstellungen des jeweiligen Wordpress/PHP-Systems. Bei Bedarf können folgende Werte in die Datei wp-admin/.htaccess bzw. php.ini eingetragen werden:
* php_value upload_max_filesize 50M
* php_value post_max_size 50M
* php_value max_execution_time 200
* php_value max_input_time 200

oder siehe http://php.net/manual/de/features.file-upload.php

= Kann man auch andere 1000° Produkte verlinken, z.B. gekaufte ePaper? =

Bei der Benutzung der Kürzel (Shortcodes) können auch bereits existierende ePapers verlinkt werden. Verwenden Sie [ePaper url=http://....] mit der URL zum ePaper auf ihrem Server.

== Changelog ==

= 1.3.3 =

* more documentation and more tests

= 1.3.2 =

* Bugfixing the refactoring and more translations

= 1.3 =

* Code Refactoring

= 1.2.2 =

* colorbox now working correctly with all themes
* new landing page http://epaper-apps.1000grad.com/

= 1.2.1 =

* now possible to upload file without pdf extension
* fixed bug with older curl versions (filename)

= 1.1.2 =

* more messages in case of local php problems on pdf-upload/curl-problems

= 1.1.0 =

* bugfix call_user_func_array, some texts and translations changed

= 1.0.5 =

* english welcome-mail, feedback form added, colorbox integration changed

= 1.0.2 =

* checks and warnings about the need of php5-curl and some more english translations

= 1.0.1 =

* warning about curl, that has to be installed on your wordpress-server

= 1.0.0 =

* plugin release

== Screenshots ==

1. Enter your registration data
2. Upload your PDF
3. Check your Details and publish to a Slot (kanalisieren)
4. Overview of published ePapers including Wizard to Post
5. Post you new ePaper to a post.
6. Use it as a wordpress-widget.
7. Enjoy your nice blog.
8. Have a look at your new ePaper.

== License ==

Our plugin is compatible with the GNU General Public License v2, or any later version.

== Translations ==

The plugin comes  in english and german language. 
Translations, please refer to the [WordPress Codex](http://codex.wordpress.org/Installing_WordPress_in_Your_Language "Installing WordPress in Your Language") for more information about activating the translation. If you want to help to translate the plugin to your language, feel free to contact us.

== curl dependency ==

* CURL has to be installed for uploading PDFs to server.
* CURL muss auf dem Wordpress System installiert sein, um PDF-Dateien laden zu können.
* <code>apt-get install php5-curl</code>

== bugs ==

This plugin may not work with a few plugins and themes. Its a beta-version! Feel free to help us and send us information about your settings and add-ons.

== Upgrade Notice ==

We're going to release periodical updates. 

== Contact ==

* 1000°DIGITAL GmbH
* Lampestr. 2
* D-04107 Leipzig
* info@epaper-apps.1000grad.com
* http://epaper-apps.1000grad.com/
    
